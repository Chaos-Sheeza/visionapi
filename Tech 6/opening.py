import cv2
from dilation import dilate
from erosion import erode

# Opening is done by eroding, then dilating.
def open(img, s_element):
    img = erode(img, s_element)
    return dilate(img, s_element)