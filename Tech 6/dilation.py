import numpy as np
import cv2

# Dilation
# Dilation works similarly to erosion, except the maximum is used instead.
def dilate(img, s_element):
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            maxpix = 0
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        maxpix = max(maxpix, pad[x+sx-1,y+sy-1])

            img[x,y] = maxpix

    return img

