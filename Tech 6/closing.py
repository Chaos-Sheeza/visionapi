import cv2
from dilation import dilate
from erosion import erode

# Closing is done by dilating, then eroding.
def close(img, s_element):
    img = dilate(img, s_element)
    return erode(img, s_element)