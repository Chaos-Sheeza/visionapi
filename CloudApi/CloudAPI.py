#The same code but made to be understood by flask
import random
from flask import Flask, jsonify, request, render_template
import cv2
import numpy as np
import math
from matplotlib import pyplot as plt
from collections import defaultdict

app = Flask(__name__)

def genDir(img):
        return "static/" + img

def retDir(img,thresh = 0):
        if thresh != 0:
                index = img.find('.')
                return "static/" + img[:index] + thresh + img[index:]

# tech 1

def binarize1(img, threshold):
    # For every element, it checks whether it's below, or above the threshold, and binarises it.
    binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)

    return jsonify(binarized)



@app.route('/binarized', methods = ['POST', 'GET'])
def binarize():
        if request.method == 'POST':
                imageDir = genDir(request.form['image'])
                img = cv2.imread(imageDir,0)
                threshold = request.form['thresh']
                # For every element, it checks whether it's below, or above the threshold, and binarises it.
                binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)
                savedir = retDir(request.form['image'], threshold)
                cv2.imwrite(savedir,binarized)
                return render_template("test.html", image = imageDir, res = savedir)
        else:
                return render_template("topost.html", name = "https://8080-dot-7282203-dot-devshell.appspot.com/binarized?authuser=0") #done to test

@app.route('/invert', methods = ['POST', 'GET'])
def invert():
        if request.method == 'POST':
                imageDir = genDir(request.form['image'])
                img = cv2.imread(imageDir,0)
                # To invert a color, you subtract its value from 255
                # A bitwise not achieves this easily
                inverted = ~img
                ret = cv2.UMat(inverted)
                savedir = retDir(request.form['image'])
                cv2.imwrite(savedir,ret)
                return render_template("test.html", image = imageDir, res = savedir)
        else:
                return render_template("topost.html", name = "https://8080-dot-7282203-dot-devshell.appspot.com/invert?authuser=0") #done to test

@app.route('/logtrans', methods = ['POST', 'GET'])
def log_transform():
        if request.method == 'POST':
                imageDir = genDir(request.form['image'])
                img = cv2.imread(imageDir,0)
                ret = np.vectorize(lambda x: np.uint8((math.log(1+x)/math.log(256))*255))(img)
                savedir = retDir(request.form['image'])
                cv2.imwrite(savedir,ret)
                return render_template("test.html", image = imageDir, res = savedir)
        else:
                return '<h>Method must be post</h>' #switching to just post methods

@app.route('/otsu', methods = ['POST'])
def otsu():
        imageDir = genDir(request.form['image'])
        img = cv2.imread(imageDir,0)
        # Retrieves an image's histogram
        imghist = cv2.calcHist([img], [0], None, [256], [0,256])

        # Gets the amount of pixels in the image
        pixels = img.shape[0] * img.shape[1]

        uniform_dist = 1.0/pixels

        fthresh = -1
        fval = -1

        for i in range(1, 255):
                # Calculates the class probabilities
                wb = np.sum(imghist[:i]) * uniform_dist
                wf = np.sum(imghist[i:]) * uniform_dist

                # Calculates the class means
                mub = np.mean(imghist[:i])
                muf = np.mean(imghist[i:])

                # Calculates the interclass variance
                val = wb * wf * (mub-muf) ** 2

                # Updates values if the variance has increased
                if val > fval:
                        fthresh = i
                        fval = val

        ret = binarize1(img, fthresh)
        savedir = retDir(request.form['image'])
        cv2.imwrite(savedir,ret)
        return render_template("test.html", image = imageDir, res = savedir)

@app.route('/powtrans', methods = ['POST'])
def pow_transform(img, power=0.5):
        imageDir = genDir(request.form['image'])
        img = cv2.imread(imageDir,0)
        ret = np.vectorize(lambda x: np.uint8(((x**power)/(255**power))*255))(img)
        savedir = retDir(request.form['image'])
        cv2.imwrite(savedir,ret)
        return render_template("test.html", image = imageDir, res = savedir)

# tech 2

def colour_hist(img, channel):
    freqs = [0] * 256

    # Updates the frequencies
    for row in img:
        for val in row:
            # Picks the right channel
            freqs[val[channel]] += 1

    return freqs


def equalise(hist):
    intensity_map = {}

    # For each input intensity, the output intensity is calculated
    # A map is used to directly map the old intensity to the new one
    for i in range(1, 257):
        sk = int(sum(hist[:i])/sum(hist)*255)
        intensity_map[i-1] = sk

    return intensity_map

@app.route('/rgbhist', methods = ['POST'])
def rgb_hist():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    rhist = colour_hist(img, 2)
    ghist = colour_hist(img, 1)
    bhist = colour_hist(img, 0) 

    # Plots and shows the frequencies
    plt.plot(rhist, color="red", linestyle="solid")
    plt.plot(ghist, color="green", linestyle="solid")
    plt.plot(bhist, color="blue", linestyle="solid")
    plt.show()
    # plot will most likely not work as an api

@app.route('/imgequalise')
def img_equalise():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    freqs = [0] * 256
    
    # Updates the frequencies
    for row in img:
        for val in row:
            freqs[val] += 1
    
    # The frequencies are equalised
    newfreqs = equalise(freqs)
    
    # Equalises the intensities in the photo
    for i in range(0, len(img)):
        for j in range(0, len(img[i])):
            img[i,j] = newfreqs[img[i,j]]
    
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,img)
    return render_template("test.html", image = imageDir, res = savedir)

@app.route('/histogram')
def histogram():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    freqs = [0] * 256

    # Updates the frequencies
    for row in img:
        for val in row:
            freqs[val] += 1

    # Plots and shows the frequencies
    plt.plot(freqs, color="red", linestyle="solid")
    plt.show()
    #once again most probably won't work in gcp

#tech 3

# Convolves the kernel to the image
def apply_kernel(img, kernel, type=np.uint8):
    # Flips kernel vertically and horizontally
    kernel = np.flip(kernel,(0,1))
    x,y = img.shape

    # Get k
    k = int(((len(kernel)-1)/2))

    # Pads the image for convolution
    pad = np.zeros((x+k*2, y+k*2))
    pad[k:-k,k:-k] = img # inserts image in padded matrix

    # Creates image-sized solution matrix
    sol = np.zeros(img.shape)

    for row in range(len(img)):
        # Prints progress
        print(f"Row {row} of {x}")
        for col in range(len(img[0])):

            sum_conv = 0

            # Offsets for kernel convolution
            k_offset = [-k,-k]

            for kcol in range(len(kernel[0])):
                for krow in range(len(kernel)):
                    # Adds kernel-cell result to convolution answer
                    sum_conv += kernel[krow,kcol] * pad[
                        row+k_offset[0]+k,
                        col+k_offset[1]+k,
                    ]

                    # Increases row offset
                    k_offset[0] += 1

                # Resets row offset
                k_offset[0] = -k

                # Increases column offset
                k_offset[1] += 1

            # Sets pixel in solution matrix
            sol[row,col] = sum_conv

    # Remove padding
    return sol.astype(type)

@app.route('/sharpen')
def sharpen(s=1):
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)

    kernel = np.array([
                [ 0, -1,  0],
                [-1,  5, -1],
                [ 0, -1,  0]
    ])

    # Sharpens according to strength
    map(lambda row: map(lambda elem: elem*s,row), kernel)
    ret = apply_kernel(img, kernel)
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

# Higher strengths may take much longer as a result.
@app.route('/smoothen')
def smoothen(s=1):
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)

    kernel = np.ndarray((1+s*2,1+s*2))
    kernel.fill(1/((1+s*2)**2))

    ret = apply_kernel(img, kernel)
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

#tech 4

def apply_threshold(img, thresh=0):
    return np.vectorize(lambda x: np.uint8(0) if x < int(thresh) else x)(img)


def generate_gaussian_kernel(size, sigma=1.4):
    size = size*2+1
    gmatrix = np.zeros((size,size))

    k = (size-1)/2
    frac = 1 / (2 * math.pi * (sigma ** 2))
    expdenom = (2 * sigma ** 2)

    for i in range(size):
        for j in range(size):
            p1 = ((i+1 - (k+1)) ** 2)
            p2 = ((j+1 - (k+1)) ** 2)
            expnom = p1 + p2
            exp = math.exp(expnom*-1/expdenom)

            gmatrix[i,j] = frac * exp

    return gmatrix

@app.route('/canny')
def canny():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    # Gaussian blur image
    # Uses a 5x5 gaussian filter with a sigma of 1.4
    gauss = generate_gaussian_kernel(2, 1.4)
    blurred_img = ka.apply_kernel(img, gauss)

    # Get magnitude of gradient and direction of gradient for each pixel
    gx = ka.apply_kernel(blurred_img, np.array([[-1,0,1],[-2,0,2],[-1,0,1]]), np.int64)
    gy = ka.apply_kernel(blurred_img, np.array([[-1,-2,-1],[0,0,0],[1,2,1]]), np.int64)
    g = np.array([
        [(x**2+y**2)**0.5 for x,y in zip(xrow,yrow)] 
        for xrow,yrow in zip(gx,gy)
    ])
    theta = np.array([
        [abs(round((math.atan2(y,x)/math.pi)*4)*45) for x,y in zip(xrow,yrow)]
        for xrow,yrow in zip(gx,gy)
    ])

    # Perform non-maximum suppression
    ## Prepare output matrix
    nms = np.zeros(img.shape)

    ## Pad g to check neighbouring pixels
    g_x,g_y = g.shape
    pad = np.zeros((g_x+2, g_y+2))
    pad[1:-1,1:-1] = g

    ## Dictionary to show relative pixels to check for each angle
    ## Each tuple is as follows:
    ##      
    ##      (x,y) 
    ##
    ## The tuple list shows which neighbouring pixels to check.
    direction = {
        0   : [( 0,-1),( 0, 1)],  ## Check above and below
        45  : [( 1, 1),(-1,-1)],  ## Check top-right and bottom-left
        90  : [(-1, 0),( 1, 0)],  ## Check left and right
        135 : [(-1, 1),( 1,-1)],  ## Check top-left and bottom-right
        180 : [( 0,-1),( 0, 1)]  ## Check above and below
    }

    for row in range(len(nms)):
        for col in range(len(nms[0])):

            ### Get respective direction for angle
            d1, d2 = direction[theta[row,col]]

            ### Get pixel coordinates to check
            pc   = (row,col)               
            pc1  = (row+d1[1], col+d1[0])  
            pc2  = (row+d2[1], col+d2[0]) 

            ### Get pixels to check
            p   = pad[pc[0]+1, pc[1]+1]
            p1  = pad[pc1[0]+1, pc1[1]+1]
            p2  = pad[pc2[0]+1, pc2[1]+1]

            ### If the current pixel is the maximal edge, keep it
            if p > p1 and p > p2 : nms[row,col] = p

            ### Note: if it isn't, it's suppressed (ignored as 0)

    ## Double thresholding
    ## Require a max and min threshold for this case
    mint = 20
    maxt = 100

    for row in range(len(nms)):
        for col in range(len(nms[0])):
            if nms[row,col] > maxt:
                nms[row,col] = 255
                pad[row+1,col+1] = 255
            elif nms[row,col] < mint:
                nms[row,col] = 0
                pad[row+1,col+1] = 0
            else:
                nms[row,col] = 128
                pad[row+1,col+1] = 128

    ## Edge tracking by hysteresis
    for row in range(len(nms)):
        for col in range(len(nms[0])):
            if nms[row,col] != 128: continue # skip weak edges

            # Check area around pixel
            area = pad[row-1:row+2, col-1:col+2]

            # If strong edge is near pixel, mark as strong edge
            # Else, suppress
            if np.any(area == 255):
                nms[row,col] = 255
            else:
                nms[row,col] = 0


    ret = nms.astype(np.uint8)
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

@app.route('/sobel')
def sobel():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    # Gets horizontal derivative of image
    hori = apply_kernel(img, np.array([
        [-1, 0, 1],
        [-2, 0, 2],
        [-1, 0, 1]
    ]), np.int64)

    # Gets vertical derivative of image
    vert = apply_kernel(img, np.array([
        [-1,-2,-1],
        [ 0, 0, 0],
        [ 1, 2, 1]
    ]), np.int64)

    # Prepares solution image matrix
    sol = np.zeros(img.shape, dtype=np.uint8)

    # Calculates final derivative of image
    for row in range(len(sol)):
        for col in range(len(sol[0])):
            sol[row,col] = (hori[row,col]**2 + vert[row,col]**2) ** 0.5

    # Applies threshold
    ret = apply_threshold(sol, 70)
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

@app.route('/segments')
def segment(threshold=150):
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    # Binarize image
    binimg = binarize(img, threshold)
    x,y = binimg.shape

    pad = np.zeros((x+2,y+2))
    pad[1:-1,1:-1] = binimg

    currlabel = 1

    labels = np.zeros((x,y), dtype=np.uint64)

    # Initialise output image
    final = np.ndarray((x,y,3))

    colours = {
        0 : [0,0,0]
    }

    linked = defaultdict(set)

    # First scan through image
    for row in range(x):
        for col in range(y):
            # Skip background pixels
            if binimg[row,col] == 0: continue
            val = binimg[row,col]
            neighbours = []

            # Add neighbouring foreground pixels to label list
            if pad[row,col]   == val: neighbours += [labels[row-1,col-1]]
            if pad[row,col+1] == val: neighbours += [labels[row-1,col]]
            if pad[row,col+2] == val: neighbours += [labels[row-1,col+1]]
            if pad[row+1,col] == val: neighbours += [labels[row,col-1]]

            # If there are no neighbours, assign a new label
            if len(neighbours) == 0:
                labels[row,col] = currlabel
                linked[currlabel].update({currlabel})
                colours[currlabel] = [random.randint(0,255),random.randint(0,255),random.randint(0,255)]
                currlabel += 1
                continue

            # Set label to minimum label of neighbourhood
            labels[row,col] = min(neighbours)

            # Update label equivalence for each neighbouring label
            for neighbour in neighbours:
                linked[neighbour].update(neighbours)

    # Updates labels to the minimum equivalent
    for row in range(x):
        for col in range(y):
            if labels[row,col] == 0: continue
            
            # Iterates over keys in order of insertion.
            # Forces labels to be associated with their minimum equivalent
            for key in linked.keys():
                if labels[row,col] in linked[key]:
                    labels[row,col] = key

    # Changes labels from integers to colours
    for row in range(x):
        for col in range(y):            
            final[row,col] = colours[labels[row,col]]

    # Prints image as coloured segments
    ret = final.astype(np.uint8)
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

@app.route('/erode')
def erode():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    s_element = request.form['tresh']  #to change
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            minpix = 255
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        minpix = min(minpix, pad[x+sx-1,y+sy-1])

            img[x,y] = minpix

    ret = img
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

@app.route('/dilate')
def dilate():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    s_element = request.form['tresh']  #to change
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            maxpix = 0
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        maxpix = max(maxpix, pad[x+sx-1,y+sy-1])

            img[x,y] = maxpix

    ret = img
    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

def dilate(img, s_element):
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            maxpix = 0
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        maxpix = max(maxpix, pad[x+sx-1,y+sy-1])

            img[x,y] = maxpix

    return img

def erode(img, s_element):
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            minpix = 255
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        minpix = min(minpix, pad[x+sx-1,y+sy-1])

            img[x,y] = minpix

    return img

@app.route('/open')
def open():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    s_element = request.form['tresh']  #to change

    img = erode(img, s_element)
    ret = dilate(img, s_element)

    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

@app.route('/close')
def close():
    imageDir = genDir(request.form['image'])
    img = cv2.imread(imageDir,0)
    s_element = request.form['tresh']  #to change

    img = dilate(img, s_element)
    ret = erode(img, s_element)

    savedir = retDir(request.form['image'])
    cv2.imwrite(savedir,ret)
    return render_template("test.html", image = imageDir, res = savedir)

if __name__ == '__main__':
        app.run(host='127.0.0.1', port=8080, debug=True)