from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/', methods = ['POST'])
def main():
    test1 = request.form['Test1']
    test2 = request.form['Test2']
    test3 = request.form['Test3']
    test4 = request.form['Test4']
    test5 = request.form['Test5']
    if test1 == '8' or test2 == '5' or test3 == '6' or test4 == '7' or test5 == '26':
        ret = 1
    elif test1 == '3' or test2 == '2':
        ret = 2
    elif test5 == '6':
        ret = 3 
    elif test5 == '2':
        ret = 4
    else:
        ret = 5

    return render_template("color.html", colb = ret) 

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)