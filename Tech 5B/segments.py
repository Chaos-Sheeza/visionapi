import cv2
import numpy as np
import random
from collections import defaultdict

def binarize(img, threshold):
    # For every element, it checks whether it's below, or above the threshold, and binarises it.
    binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)

    return binarized

# Splits image into segments, giving each one a colour
@app.route('/segments')
def segment(img, threshold=150):
    # Binarize image
    binimg = binarize(img, threshold)
    x,y = binimg.shape

    pad = np.zeros((x+2,y+2))
    pad[1:-1,1:-1] = binimg

    currlabel = 1

    labels = np.zeros((x,y), dtype=np.uint64)

    # Initialise output image
    final = np.ndarray((x,y,3))

    colours = {
        0 : [0,0,0]
    }

    linked = defaultdict(set)

    # First scan through image
    for row in range(x):
        for col in range(y):
            # Skip background pixels
            if binimg[row,col] == 0: continue
            val = binimg[row,col]
            neighbours = []

            # Add neighbouring foreground pixels to label list
            if pad[row,col]   == val: neighbours += [labels[row-1,col-1]]
            if pad[row,col+1] == val: neighbours += [labels[row-1,col]]
            if pad[row,col+2] == val: neighbours += [labels[row-1,col+1]]
            if pad[row+1,col] == val: neighbours += [labels[row,col-1]]

            # If there are no neighbours, assign a new label
            if len(neighbours) == 0:
                labels[row,col] = currlabel
                linked[currlabel].update({currlabel})
                colours[currlabel] = [random.randint(0,255),random.randint(0,255),random.randint(0,255)]
                currlabel += 1
                continue

            # Set label to minimum label of neighbourhood
            labels[row,col] = min(neighbours)

            # Update label equivalence for each neighbouring label
            for neighbour in neighbours:
                linked[neighbour].update(neighbours)

    # Updates labels to the minimum equivalent
    for row in range(x):
        for col in range(y):
            if labels[row,col] == 0: continue
            
            # Iterates over keys in order of insertion.
            # Forces labels to be associated with their minimum equivalent
            for key in linked.keys():
                if labels[row,col] in linked[key]:
                    labels[row,col] = key

    # Changes labels from integers to colours
    for row in range(x):
        for col in range(y):            
            final[row,col] = colours[labels[row,col]]

    # Prints image as coloured segments
    cv2.imshow('img', final.astype(np.uint8))
    cv2.waitKey(0)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)