import cv2
import numpy as np

def binarize(img, threshold):
    # For every element, it checks whether it's below, or above the threshold, and binarises it.
    binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)

    return binarized

# cv2.imshow('image', binarized)
# cv2.waitKey()