import cv2
import numpy as np
from flask import Flask

app = Flask(__name__)

@app.route('/')
def invert(img):
    # To invert a color, you subtract its value from 255
    # A bitwise not achieves this easily
    inverted = ~img

    return cv2.UMat(inverted)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)
        