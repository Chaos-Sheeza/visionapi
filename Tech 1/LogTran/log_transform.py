import cv2
import math
import numpy as np
from flask import Flask

app = Flask(__name__)

@app.route('/')
def log_transform(img):
    # Performs the log transform on the image
    # It gets a value from 0 to 1 by dividing log(1+x) with the max value log(256)
    # Then it multiplies it with 256 to get the colour back
    return np.vectorize(lambda x: np.uint8((math.log(1+x)/math.log(256))*256))(img)

if __name__ == '__log_transform__':
        app.run(host='127.0.0.1', port=8080, debug=True)
        