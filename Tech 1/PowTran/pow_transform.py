import cv2
import math
import numpy as np
from flask import Flask

app = Flask(__name__)

@app.route('/')
def pow_transform(img, power=0.5):
    # Performs the power transform on the image
    # Different powers produce different outputs
    return np.vectorize(lambda x: np.uint8(((x**power)/(255**power))*255))(img)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)