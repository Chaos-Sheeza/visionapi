import cv2
import numpy as np
#from binarization import binarize
from flask import Flask

app = Flask(__name__)

def binarize(img, threshold):
    # For every element, it checks whether it's below, or above the threshold, and binarises it.
    binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)

    return binarized

@app.route('/')
def otsu(img):
    # Retrieves an image's histogram
    imghist = cv2.calcHist([img], [0], None, [256], [0,256])

    # Gets the amount of pixels in the image
    pixels = img.shape[0] * img.shape[1]

    uniform_dist = 1.0/pixels

    fthresh = -1
    fval = -1

    for i in range(1, 255):
        # Calculates the class probabilities
        wb = np.sum(imghist[:i]) * uniform_dist
        wf = np.sum(imghist[i:]) * uniform_dist

        # Calculates the class means
        mub = np.mean(imghist[:i])
        muf = np.mean(imghist[i:])

        # Calculates the interclass variance
        val = wb * wf * (mub-muf) ** 2

        # Updates values if the variance has increased
        if val > fval:
            fthresh = i
            fval = val

    return binarize(img, fthresh)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)