import cv2
import numpy as np
from flask import Flask

app = Flask(__name__)

@app.route('/')
def binarize(img, threshold):
    # For every element, it checks whether it's below, or above the threshold, and binarises it.
    binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)

    return binarized

if __name__ == '__binarization__':
        app.run(host='127.0.0.1', port=8080, debug=True)
        
# cv2.imshow('image', binarized)
# cv2.waitKey()