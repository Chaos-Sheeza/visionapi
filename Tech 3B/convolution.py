import cv2
import numpy as np
from flask import Flask

app = Flask(__name__)

# Sharpens image using a kernel
@app.route('/sharpen')
def sharpen(img, s=1):
    kernel = np.array([
                [ 0, -1,  0],
                [-1,  5, -1],
                [ 0, -1,  0]
    ])

    # Sharpens according to strength
    map(lambda row: map(lambda elem: elem*s,row), kernel)

    return apply_kernel(img, kernel)

# Smoothens an image according to the strength
# Note: the kernel's size will be (1+s*2) x (1+s*2)
#
# Higher strengths may take much longer as a result.
@app.route('/smoothen')
def smoothen(img, s=1):
    kernel = np.ndarray((1+s*2,1+s*2))
    kernel.fill(1/((1+s*2)**2))

    return apply_kernel(img, kernel)

# Convolves the kernel to the image
def apply_kernel(img, kernel, type=np.uint8):
    # Flips kernel vertically and horizontally
    kernel = np.flip(kernel,(0,1))
    x,y = img.shape

    # Get k
    k = int(((len(kernel)-1)/2))

    # Pads the image for convolution
    pad = np.zeros((x+k*2, y+k*2))
    pad[k:-k,k:-k] = img # inserts image in padded matrix

    # Creates image-sized solution matrix
    sol = np.zeros(img.shape)

    for row in range(len(img)):
        # Prints progress
        print(f"Row {row} of {x}")
        for col in range(len(img[0])):

            sum_conv = 0

            # Offsets for kernel convolution
            k_offset = [-k,-k]

            for kcol in range(len(kernel[0])):
                for krow in range(len(kernel)):
                    # Adds kernel-cell result to convolution answer
                    sum_conv += kernel[krow,kcol] * pad[
                        row+k_offset[0]+k,
                        col+k_offset[1]+k,
                    ]

                    # Increases row offset
                    k_offset[0] += 1

                # Resets row offset
                k_offset[0] = -k

                # Increases column offset
                k_offset[1] += 1

            # Sets pixel in solution matrix
            sol[row,col] = sum_conv

    # Remove padding
    return sol.astype(type)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)