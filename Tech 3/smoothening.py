import cv2
import numpy as np
import kernel_apply as ka

# Smoothens an image according to the strength
# Note: the kernel's size will be (1+s*2) x (1+s*2)
#
# Higher strengths may take much longer as a result.
def smoothen(img, s=1):
    kernel = np.ndarray((1+s*2,1+s*2))
    kernel.fill(1/((1+s*2)**2))

    return ka.apply_kernel(img, kernel)