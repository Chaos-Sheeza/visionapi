import cv2
import numpy as np
import kernel_apply as ka

# Sharpens image using a kernel
def sharpen(img, s=1):
    kernel = np.array([
                [ 0, -1,  0],
                [-1,  5, -1],
                [ 0, -1,  0]
    ])

    # Sharpens according to strength
    map(lambda row: map(lambda elem: elem*s,row), kernel)

    return ka.apply_kernel(img, kernel)