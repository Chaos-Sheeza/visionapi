import numpy as np
import cv2

# Erosion
# In Erosion, you have the image matrix A and the "structuring element" B
# The structuring element is made out of binary elements 0 and 1.
#       Where each 1 shows the shape of the structuring element.
# Each pixel of the image is set to the minimum of the element's shape.
# s_element needs to be an (N x N) matrix that can be converted into a numpy array.
# This conversion is done in-function to help the API abstract from numpy.
@app.route('/erode')
def erode(img, s_element):
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            minpix = 255
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        minpix = min(minpix, pad[x+sx-1,y+sy-1])

            img[x,y] = minpix

    return img

# Dilation
# Dilation works similarly to erosion, except the maximum is used instead.
@app.route('/dilate')
def dilate(img, s_element):
    ix,iy = img.shape
    pad = np.zeros((ix+2,iy+2)) # pad by 1x1 additional space
    pad[1:-1,1:-1] = img

    s_element = np.array(s_element)

    for x in range(len(img)):
        for y in range(len(img[0])):
            maxpix = 0
            for sx in range(len(s_element)):
                for sy in range(len(s_element[0])):
                    if s_element[sx,sy] == 1:
                        maxpix = max(maxpix, pad[x+sx-1,y+sy-1])

            img[x,y] = maxpix

    return img

# Opening is done by eroding, then dilating.
@app.route('/open')
def open(img, s_element):
    img = erode(img, s_element)
    return dilate(img, s_element)

# Closing is done by dilating, then eroding.
@app.route('/close')
def close(img, s_element):
    img = dilate(img, s_element)
    return erode(img, s_element)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)