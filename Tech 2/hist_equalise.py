import cv2
import numpy as np
from matplotlib import pyplot as plt

def equalise(hist):
    intensity_map = {}

    # For each input intensity, the output intensity is calculated
    # A map is used to directly map the old intensity to the new one
    for i in range(1, 257):
        sk = int(sum(hist[:i])/sum(hist)*255)
        intensity_map[i-1] = sk

    return intensity_map

# Equalises an image according to its histogram
def img_equalise(img):
    freqs = [0] * 256
    
    # Updates the frequencies
    for row in img:
        for val in row:
            freqs[val] += 1
    
    # The frequencies are equalised
    newfreqs = equalise(freqs)
    
    # Equalises the intensities in the photo
    for i in range(0, len(img)):
        for j in range(0, len(img[i])):
            img[i,j] = newfreqs[img[i,j]]
    
    return img