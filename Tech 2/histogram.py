import cv2
import numpy as np
from matplotlib import pyplot as plt

def histogram(img):
    freqs = [0] * 256

    # Updates the frequencies
    for row in img:
        for val in row:
            freqs[val] += 1

    # Plots and shows the frequencies
    plt.plot(freqs, color="red", linestyle="solid")
    plt.show()