import cv2
import numpy as np
from matplotlib import pyplot as plt

def colour_hist(img, channel):
    freqs = [0] * 256

    # Updates the frequencies
    for row in img:
        for val in row:
            # Picks the right channel
            freqs[val[channel]] += 1

    return freqs

# Note!
# The image needs to be loaded in BGR - otherwise the channels will be mixed.
# Loading the image normally loads it in BGR, but if that doesn't happen, then convert.
def rgb_hist(img):
    rhist = colour_hist(img, 2)
    ghist = colour_hist(img, 1)
    bhist = colour_hist(img, 0) 

    # Plots and shows the frequencies
    plt.plot(rhist, color="red", linestyle="solid")
    plt.plot(ghist, color="green", linestyle="solid")
    plt.plot(bhist, color="blue", linestyle="solid")
    plt.show()