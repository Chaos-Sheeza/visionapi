import cv2
import numpy as np
from matplotlib import pyplot as plt
from flask import Flask

app = Flask(__name__)

def colour_hist(img, channel):
    freqs = [0] * 256

    # Updates the frequencies
    for row in img:
        for val in row:
            # Picks the right channel
            freqs[val[channel]] += 1

    return freqs

# Note!
# The image needs to be loaded in BGR - otherwise the channels will be mixed.
# Loading the image normally loads it in BGR, but if that doesn't happen, then convert.
@app.route('/rgbhist')
def rgb_hist(img):
    rhist = colour_hist(img, 2)
    ghist = colour_hist(img, 1)
    bhist = colour_hist(img, 0) 

    # Plots and shows the frequencies
    plt.plot(rhist, color="red", linestyle="solid")
    plt.plot(ghist, color="green", linestyle="solid")
    plt.plot(bhist, color="blue", linestyle="solid")
    plt.show()

def equalise(hist):
    intensity_map = {}

    # For each input intensity, the output intensity is calculated
    # A map is used to directly map the old intensity to the new one
    for i in range(1, 257):
        sk = int(sum(hist[:i])/sum(hist)*255)
        intensity_map[i-1] = sk

    return intensity_map

# Equalises an image according to its histogram
@app.route('/imgequalise')
def img_equalise(img):
    freqs = [0] * 256
    
    # Updates the frequencies
    for row in img:
        for val in row:
            freqs[val] += 1
    
    # The frequencies are equalised
    newfreqs = equalise(freqs)
    
    # Equalises the intensities in the photo
    for i in range(0, len(img)):
        for j in range(0, len(img[i])):
            img[i,j] = newfreqs[img[i,j]]
    
    return img

@app.route('/histogram')
def histogram(img):
    freqs = [0] * 256

    # Updates the frequencies
    for row in img:
        for val in row:
            freqs[val] += 1

    # Plots and shows the frequencies
    plt.plot(freqs, color="red", linestyle="solid")
    plt.show()

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)