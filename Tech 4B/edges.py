import cv2
import math
import numpy as np
import kernel_apply as ka

# Applies a threshold on the image
# Abstracted due to the complexity of the line.
# Sets pixel to 0 if below the threshold, else it's set to itself.
def apply_threshold(img, thresh=0):
    return np.vectorize(lambda x: np.uint8(0) if x < int(thresh) else x)(img)

# size is a single number x for which the kernel size is (x,x)
# followed formula from https://en.wikipedia.org/wiki/Canny_edge_detector#Process_of_Canny_edge_detection_algorithm
def generate_gaussian_kernel(size, sigma=1.4):
    size = size*2+1
    gmatrix = np.zeros((size,size))

    k = (size-1)/2
    frac = 1 / (2 * math.pi * (sigma ** 2))
    expdenom = (2 * sigma ** 2)

    for i in range(size):
        for j in range(size):
            p1 = ((i+1 - (k+1)) ** 2)
            p2 = ((j+1 - (k+1)) ** 2)
            expnom = p1 + p2
            exp = math.exp(expnom*-1/expdenom)

            gmatrix[i,j] = frac * exp

    return gmatrix

@app.route('/canny')
def canny(img):
    # Gaussian blur image
    # Uses a 5x5 gaussian filter with a sigma of 1.4
    gauss = generate_gaussian_kernel(2, 1.4)
    blurred_img = ka.apply_kernel(img, gauss)

    # Get magnitude of gradient and direction of gradient for each pixel
    gx = ka.apply_kernel(blurred_img, np.array([[-1,0,1],[-2,0,2],[-1,0,1]]), np.int64)
    gy = ka.apply_kernel(blurred_img, np.array([[-1,-2,-1],[0,0,0],[1,2,1]]), np.int64)
    g = np.array([
        [(x**2+y**2)**0.5 for x,y in zip(xrow,yrow)] 
        for xrow,yrow in zip(gx,gy)
    ])
    theta = np.array([
        [abs(round((math.atan2(y,x)/math.pi)*4)*45) for x,y in zip(xrow,yrow)]
        for xrow,yrow in zip(gx,gy)
    ])

    # Perform non-maximum suppression
    ## Prepare output matrix
    nms = np.zeros(img.shape)

    ## Pad g to check neighbouring pixels
    g_x,g_y = g.shape
    pad = np.zeros((g_x+2, g_y+2))
    pad[1:-1,1:-1] = g

    ## Dictionary to show relative pixels to check for each angle
    ## Each tuple is as follows:
    ##      
    ##      (x,y) 
    ##
    ## The tuple list shows which neighbouring pixels to check.
    direction = {
        0   : [( 0,-1),( 0, 1)],  ## Check above and below
        45  : [( 1, 1),(-1,-1)],  ## Check top-right and bottom-left
        90  : [(-1, 0),( 1, 0)],  ## Check left and right
        135 : [(-1, 1),( 1,-1)],  ## Check top-left and bottom-right
        180 : [( 0,-1),( 0, 1)]  ## Check above and below
    }

    for row in range(len(nms)):
        for col in range(len(nms[0])):

            ### Get respective direction for angle
            d1, d2 = direction[theta[row,col]]

            ### Get pixel coordinates to check
            pc   = (row,col)               
            pc1  = (row+d1[1], col+d1[0])  
            pc2  = (row+d2[1], col+d2[0]) 

            ### Get pixels to check
            p   = pad[pc[0]+1, pc[1]+1]
            p1  = pad[pc1[0]+1, pc1[1]+1]
            p2  = pad[pc2[0]+1, pc2[1]+1]

            ### If the current pixel is the maximal edge, keep it
            if p > p1 and p > p2 : nms[row,col] = p

            ### Note: if it isn't, it's suppressed (ignored as 0)

    ## Double thresholding
    ## Require a max and min threshold for this case
    mint = 20
    maxt = 100

    for row in range(len(nms)):
        for col in range(len(nms[0])):
            if nms[row,col] > maxt:
                nms[row,col] = 255
                pad[row+1,col+1] = 255
            elif nms[row,col] < mint:
                nms[row,col] = 0
                pad[row+1,col+1] = 0
            else:
                nms[row,col] = 128
                pad[row+1,col+1] = 128

    ## Edge tracking by hysteresis
    for row in range(len(nms)):
        for col in range(len(nms[0])):
            if nms[row,col] != 128: continue # skip weak edges

            # Check area around pixel
            area = pad[row-1:row+2, col-1:col+2]

            # If strong edge is near pixel, mark as strong edge
            # Else, suppress
            if np.any(area == 255):
                nms[row,col] = 255
            else:
                nms[row,col] = 0


    return nms.astype(np.uint8)

# Performs the sobel edge detection using convolutions and formulae
@app.route('/sobel')
def sobel(img):
    # Gets horizontal derivative of image
    hori = ka.apply_kernel(img, np.array([
        [-1, 0, 1],
        [-2, 0, 2],
        [-1, 0, 1]
    ]), np.int64)

    # Gets vertical derivative of image
    vert = ka.apply_kernel(img, np.array([
        [-1,-2,-1],
        [ 0, 0, 0],
        [ 1, 2, 1]
    ]), np.int64)

    # Prepares solution image matrix
    sol = np.zeros(img.shape, dtype=np.uint8)

    # Calculates final derivative of image
    for row in range(len(sol)):
        for col in range(len(sol[0])):
            sol[row,col] = (hori[row,col]**2 + vert[row,col]**2) ** 0.5

    # Applies threshold
    return apply_threshold(sol, 70)

# Convolves the kernel to the image
def apply_kernel(img, kernel, type=np.uint8):
    # Flips kernel vertically and horizontally
    kernel = np.flip(kernel,(0,1))
    x,y = img.shape

    # Get k
    k = int(((len(kernel)-1)/2))

    # Pads the image for convolution
    pad = np.zeros((x+k*2, y+k*2))
    pad[k:-k,k:-k] = img # inserts image in padded matrix

    # Creates image-sized solution matrix
    sol = np.zeros(img.shape)

    for row in range(len(img)):
        # Prints progress
        print(f"Row {row} of {x}")
        for col in range(len(img[0])):

            sum_conv = 0

            # Offsets for kernel convolution
            k_offset = [-k,-k]

            for kcol in range(len(kernel[0])):
                for krow in range(len(kernel)):
                    # Adds kernel-cell result to convolution answer
                    sum_conv += kernel[krow,kcol] * pad[
                        row+k_offset[0]+k,
                        col+k_offset[1]+k,
                    ]

                    # Increases row offset
                    k_offset[0] += 1

                # Resets row offset
                k_offset[0] = -k

                # Increases column offset
                k_offset[1] += 1

            # Sets pixel in solution matrix
            sol[row,col] = sum_conv

    # Remove padding
    return sol.astype(type)

if __name__ == '__invert__':
        app.run(host='127.0.0.1', port=8080, debug=True)