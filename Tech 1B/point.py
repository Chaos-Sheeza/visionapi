import cv2
import math
import numpy as np
from flask import Flask
from flask import jsonify

app = Flask(__name__)

@app.route('/')
def start():
        return 'Start'

@app.route('/binarize/<img>/<int:threshold>')
def binarize(img, threshold):
    # For every element, it checks whether it's below, or above the threshold, and binarises it.
    binarized = np.vectorize(lambda x: np.uint8(0) if x < int(threshold) else np.uint8(255))(img)

    return jsonify(binarized)

@app.route('/invert/<img>')
def invert(img):
    # To invert a color, you subtract its value from 255
    # A bitwise not achieves this easily
    inverted = ~img

    return cv2.UMat(inverted)


@app.route('/logtrans')
def log_transform(img):
    # Performs the log transform on the image
    # It gets a value from 0 to 1 by dividing log(1+x) with the max value log(256)
    # Then it multiplies it with 256 to get the colour back
    return np.vectorize(lambda x: np.uint8((math.log(1+x)/math.log(256))*256))(img)


@app.route('/otsu')
def otsu(img):
    # Retrieves an image's histogram
    imghist = cv2.calcHist([img], [0], None, [256], [0,256])

    # Gets the amount of pixels in the image
    pixels = img.shape[0] * img.shape[1]

    uniform_dist = 1.0/pixels

    fthresh = -1
    fval = -1

    for i in range(1, 255):
        # Calculates the class probabilities
        wb = np.sum(imghist[:i]) * uniform_dist
        wf = np.sum(imghist[i:]) * uniform_dist

        # Calculates the class means
        mub = np.mean(imghist[:i])
        muf = np.mean(imghist[i:])

        # Calculates the interclass variance
        val = wb * wf * (mub-muf) ** 2

        # Updates values if the variance has increased
        if val > fval:
            fthresh = i
            fval = val

    return binarize(img, fthresh)


@app.route('/powtrans')
def pow_transform(img, power=0.5):
    # Performs the power transform on the image
    # Different powers produce different outputs
    return np.vectorize(lambda x: np.uint8(((x**power)/(255**power))*255))(img)

if __name__ == '__main__':
        app.run(host='127.0.0.1', port=8080, debug=True)