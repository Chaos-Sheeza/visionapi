import cv2
import numpy as np
import kernel_apply as ka
import math

# Applies a threshold on the image
# Abstracted due to the complexity of the line.
# Sets pixel to 0 if below the threshold, else it's set to itself.
def apply_threshold(img, thresh=0):
    return np.vectorize(lambda x: np.uint8(0) if x < int(thresh) else x)(img)

# Performs the sobel edge detection using convolutions and formulae
def sobel(img):
    # Gets horizontal derivative of image
    hori = ka.apply_kernel(img, np.array([
        [-1, 0, 1],
        [-2, 0, 2],
        [-1, 0, 1]
    ]), np.int64)

    # Gets vertical derivative of image
    vert = ka.apply_kernel(img, np.array([
        [-1,-2,-1],
        [ 0, 0, 0],
        [ 1, 2, 1]
    ]), np.int64)

    # Prepares solution image matrix
    sol = np.zeros(img.shape, dtype=np.uint8)

    # Calculates final derivative of image
    for row in range(len(sol)):
        for col in range(len(sol[0])):
            sol[row,col] = (hori[row,col]**2 + vert[row,col]**2) ** 0.5

    # Applies threshold
    return apply_threshold(sol, 70)