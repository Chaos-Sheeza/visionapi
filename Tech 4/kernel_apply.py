import numpy as np

# Convolves the kernel to the image
def apply_kernel(img, kernel, type=np.uint8):
    # Flips kernel vertically and horizontally
    kernel = np.flip(kernel,(0,1))
    x,y = img.shape

    # Get k
    k = int(((len(kernel)-1)/2))

    # Pads the image for convolution
    pad = np.zeros((x+k*2, y+k*2))
    pad[k:-k,k:-k] = img # inserts image in padded matrix

    # Creates image-sized solution matrix
    sol = np.zeros(img.shape)

    for row in range(len(img)):
        # Prints progress
        print(f"Row {row} of {x}")
        for col in range(len(img[0])):

            sum_conv = 0

            # Offsets for kernel convolution
            k_offset = [-k,-k]

            for kcol in range(len(kernel[0])):
                for krow in range(len(kernel)):
                    # Adds kernel-cell result to convolution answer
                    sum_conv += kernel[krow,kcol] * pad[
                        row+k_offset[0]+k,
                        col+k_offset[1]+k,
                    ]

                    # Increases row offset
                    k_offset[0] += 1

                # Resets row offset
                k_offset[0] = -k

                # Increases column offset
                k_offset[1] += 1

            # Sets pixel in solution matrix
            sol[row,col] = sum_conv

    # Remove padding
    return sol.astype(type)